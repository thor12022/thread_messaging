#ifndef THREAD_MESSAGING_THREAD_HPP
#define THREAD_MESSAGING_THREAD_HPP

#include <future>
#include <memory>
#include <shared_mutex>
#include <thread>
#include <vector>

namespace thread_messaging
{
  template<class ThreadImpl_t>
   class thread
   {
   public:
      thread() :
         m_thread{ &thread::run, this },
         m_stop_requested{ false }
      {}

      thread(ThreadImpl_t impl) :
         m_impl{ std::move(impl) },
         m_thread{ &thread::run, this },
         m_stop_requested{ false }
      {}

      ~thread()
      {
         stop();
         if(m_thread.joinable())
         {
            m_thread.join();
         }
      }

      thread(const thread&) = delete;
      thread(thread&&) = default;

      thread& operator=(const thread&) = delete;
      thread& operator=(thread&&) = delete;

      ThreadImpl_t* operator->()
      {
         return &m_impl;
      }

     template<class MessageImpl_t>
     void  post(MessageImpl_t impl)
      {
         static_assert(std::is_move_constructible<MessageImpl_t>::value);
         std::unique_lock lock(m_message_mutex);
         m_messages.emplace_back(std::move(impl));
      }

     template<class MessageImpl_t, typename Return_t = typename MessageImpl_t::Return_t>
     Return_t send(MessageImpl_t impl)
      {
         static_assert(std::is_move_constructible<MessageImpl_t>::value);

         std::promise<Return_t> promise;
         auto future_result{ promise.get_future() };

         {
            std::unique_lock lock(m_message_mutex);
            m_messages.emplace(m_messages.begin(), std::move(impl), std::move(promise));
         }
         return future_result.get();

      }

      void stop()
      {
         m_stop_requested = true;
      }

   private:
      struct message_interface
      {
         virtual ~message_interface() = default;

         virtual void process(ThreadImpl_t& thread) = 0;
      };

      template<class MessageImpl_t>
      class message : public message_interface
      {
      public:
         message() = default;

         message(MessageImpl_t impl) :
            m_impl{ std::move(impl) }
         {}

         virtual ~message() = default;

         message(const message&) = delete;
         message(message&&) noexcept = default;

         message& operator=(const message&) = delete;
         message& operator=(message&&) = default;

         virtual void process(ThreadImpl_t& thread) override
         {
            thread.process(std::move(m_impl));
         }

      private:
         MessageImpl_t m_impl;
      };

      template<class MessageImpl_t, typename Return_t>
      class message_returnable : public message_interface
      {
      public:
         message_returnable() = delete;

         message_returnable(MessageImpl_t impl, std::promise<Return_t> promise) :
            m_impl{ std::move(impl) },
            m_promise{ std::move(promise) }
         {}

         virtual ~message_returnable() = default;

         message_returnable(const message_returnable&) = delete;
         message_returnable(message_returnable&&) noexcept = default;

         message_returnable& operator=(const message_returnable&) = delete;
         message_returnable& operator=(message_returnable&&) = default;

         virtual void process(ThreadImpl_t& thread) override
         {
            m_promise.set_value(thread.process(std::move(m_impl)));
         }

      private:
         MessageImpl_t m_impl;
         std::promise<Return_t> m_promise;
      };

      class message_wrapper
      {
      public:
         message_wrapper() = delete;

         template<class MessageImpl_t>
         message_wrapper(MessageImpl_t impl) :
            m_contents{ new message<MessageImpl_t>(std::move(impl)) }
         {}

         template<class MessageImpl_t, typename Return_t>
         message_wrapper(MessageImpl_t impl, std::promise<Return_t> promise) :
            m_contents{ new message_returnable<MessageImpl_t, Return_t>(std::move(impl), std::move(promise)) }
         {}

         virtual ~message_wrapper() = default;

         message_wrapper(const message_wrapper&) = delete;
         message_wrapper(message_wrapper&&) noexcept = default;

         message_wrapper& operator=(const message_wrapper&) = delete;
         message_wrapper& operator=(message_wrapper&&) = default;

         void process(ThreadImpl_t& thread)
         {
            m_contents->process(thread);
         }

      private:
         std::unique_ptr<message_interface> m_contents;
      };

      void run()
      {
         while(!m_stop_requested && !m_impl.finished())
         {
            process_messages();
            m_impl.step();
         }
      }

      void process_messages()
      {
         auto lock{ std::unique_lock( m_message_mutex ) };

         for( auto& message : m_messages)
         {
            message.process(m_impl);
         }
         m_messages.clear();
      }

      std::vector<message_wrapper> m_messages;
      std::shared_mutex  m_message_mutex;
      ThreadImpl_t  m_impl;
      std::thread m_thread;
      bool m_stop_requested;
   };
}

#endif
