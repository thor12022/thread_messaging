cmake_minimum_required(VERSION 3.9)

add_executable(thread_messaging_test
   test.cpp)
   
target_link_libraries(thread_messaging_test
   thread_messaging_includes
   catch)
   
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(thread_messaging_test Threads::Threads)

set_property(TARGET thread_messaging_test PROPERTY CXX_STANDARD 17)
