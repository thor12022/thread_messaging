#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <thread_messaging/thread.hpp>

#include <chrono>
#include <vector>

struct TestMessage
{
   TestMessage(bool& received)
      : m_received{ received }
   {}

   TestMessage() = delete;
   ~TestMessage() = default;
   TestMessage(const TestMessage&) = delete;
   TestMessage(TestMessage&&) = default;
   
   TestMessage& operator=(const TestMessage&) = delete;
   TestMessage& operator=(TestMessage&&) = delete;
   
   bool& m_received;
};

class TestThread
{
public:
   TestThread() :
      m_test{ 0 }
   {}
   
   TestThread(int val) :
      m_test{ val }
   {}
   
   ~TestThread() = default;

   int test()
   {
      return m_test;
   }
   
   void step()
   {
      m_test++;
   }
   
    bool finished()
   {
      return false;
   }
   
   void process(TestMessage message)
   {
      message.m_received = true;
   }
   
private:
   int m_test;
};


TEST_CASE("Thread creation without crashing")
{
   using namespace std::chrono_literals;
   SECTION("Default Constuctor")
   {
      thread_messaging::thread<TestThread> thread;
      std::this_thread::sleep_for(200ms);
      
      REQUIRE(thread->test() > 0);
   }
   
   SECTION("Other Constructor")
   {
      thread_messaging::thread<TestThread> thread( 1 );
      std::this_thread::sleep_for(200ms);
      
      REQUIRE(thread->test() > 1);
   }
}

TEST_CASE("Post Message to Thread")
{
   using namespace std::chrono_literals;
   thread_messaging::thread<TestThread> thread;
   
   bool message_received{ false };
   thread.post(TestMessage(message_received));
   std::this_thread::sleep_for(250ms);
   
   REQUIRE(message_received == true);
   
}

struct PassingMessage
{
   PassingMessage()
   {}
   
   PassingMessage(const PassingMessage&) = delete;
   PassingMessage& operator=(const PassingMessage&) = delete;
   
   PassingMessage(PassingMessage&&) = default;
   PassingMessage& operator=(PassingMessage&&) = default;
   
   ~PassingMessage() = default;
   
   int m_value = 0;

   using Return_t = int;
};

class PassingThread
{
public:
   PassingThread() = default;
   ~PassingThread() = default;

   void setNext(thread_messaging::thread<PassingThread>& thread)
   {
      m_next = &thread;
   }
   
   void process( PassingMessage msg )
   {
      if(++msg.m_value < 16)
      {
         m_next->post(std::move(msg));
      }
      else
      {
         m_finished = true;
      }
   }
   
   void step()
   {}
   
   bool finished()
   {
      return m_finished;
   }
   
   thread_messaging::thread<PassingThread>* m_next;
   bool m_finished = false;
};

SCENARIO("Posting Messages between threads")
{
   using namespace std::chrono_literals;
   std::vector<thread_messaging::thread<PassingThread>> threads(16);
   
   if(threads.size() == 1)
   {
      threads[0]->setNext(threads[0]);
   }
   else if( threads.size() > 1)
   {
      for(auto thread1{ threads.begin() }, thread2{ ++threads.begin() }; thread2 != threads.end(); thread1++, thread2++)
      {
         (*thread1)->setNext(*thread2);
      }
      threads.back()->setNext(threads.front());
   }
   
   threads[0].post( PassingMessage() );
   
   std::this_thread::sleep_for(5s);
   
   REQUIRE(std::any_of(threads.begin(), threads.end(), [](auto&& iter){return iter->finished();}));   
}

class SendThread
{
public:
   SendThread() = default;
   ~SendThread() = default;

   PassingMessage::Return_t process( PassingMessage msg )
   {
      m_finished = true;

      return ++msg.m_value;
   }
   
   void step()
   {}
   
   bool finished()
   {
      return m_finished;
   }
  
private:
   bool m_finished = false;
};

SCENARIO("Sending Messages between threads")
{
   using namespace std::chrono_literals;
   thread_messaging::thread<SendThread> thread;

   auto result{ thread.send(PassingMessage()) };

   std::cout << result << std::endl;

   REQUIRE(result == 1);
}
